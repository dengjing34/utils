<?php
/**
 * This file is generated automatically by ConfigurationSystem.
 * Do not change it manually in production, unless you know what you're doing and can take responsibilities for the consequences of changes you make.
 */


namespace Config;

class MNLogger
{
    public $exception = array(
        'on' => true,
        'app' => 'Utils',
        'logdir' => '/tmp/logs/monitor/'
    );
    public $trace = array(
        'on' => true,
        'app' => 'Utils',
        'logdir' => '/tmp/logs/monitor/'
    );
    public $data = array(
        'on' => true,
        'app' => 'Utils',
        'logdir' => '/tmp/logs/monitor/'
    );
}
