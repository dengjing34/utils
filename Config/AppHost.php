<?php
namespace Config;

class AppHost
{

    /**
     * App应用和域名的映射关系.
     *
     * @var array
     */
    public static $mapping = array(
        'shuabu' => array('shuabuapp.com', 'sxs0.com'),
        'zhuanbu' => array('izhuanbu.com'),
    );

}