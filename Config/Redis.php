<?php
/**
 * This file is generated automatically by ConfigurationSystem.
 * Do not change it manually in production, unless you know what you're doing and can take responsibilities for the consequences of changes you make.
 */

namespace Config;

$pool_enable = false;
if (!$pool_enable) {
    // 没替换的情况默认为开启.
    if (!defined('JM_PHP_CONN_POOL_ON')) {
        define('JM_PHP_CONN_POOL_ON', false);
    }
}

class Redis{
    /**
     * Configs of Redis.
     * @var array
     */
    public $default = array(
        'nodes' => array (
  0 => 
  array (
    'master' => '127.0.0.1:6379',
    'master-alia' => '127.0.0.1:6379',
  ),
),
        'db' => 0
    );

}