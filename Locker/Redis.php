<?php
/**
 * Redis Locker.
 *
 * @author dengjing <jingd3@jumei.com>
 */

namespace Utils\Locker;

/**
 * Redis Locker.
 */
class Redis extends \Utils\Singleton
{

    /**
     * 分区标识.
     *
     * @var integer
     */
    protected $partition;

    /**
     * Get instance of the derived class.
     *
     * @return \Utils\Locker\Redis
     */
    public static function instance()
    {
        return parent::instance();
    }

    /**
     * 通过redis锁定一个需要执行的key, 注意key的长度不要超过.
     *
     * @param string  $prefix      锁前缀.
     * @param string  $key         锁的key.
     * @param integer $expireAfter 多久过期,默认为0,即300秒(5分钟)后过期.
     *
     * @return boolean 锁定成功返回true,锁定失败返回false.
     */
    public function lock($prefix, $key, $expireAfter = 0)
    {
        $partition = $this->getPartition();// 保险起见在第一行就重置$this->partition为null.
        $result = false;
        $timestamp = time();
        $expireAfterSeconds = $expireAfter > 0 ? $expireAfter : \Config\Locker::$redis['ttl'];
        $realExpireAfterSeconds = $expireAfterSeconds + 1; // 实际会在多少秒之后过期.
        $expireAt = $timestamp + $realExpireAfterSeconds; // 在哪个时间戳过期.
        $redisKey = $this->genKey($prefix, $key);
        $redisCfgName = \Config\Locker::$redis['name'];
        if (is_null($partition)) {
            $redis = \Redis\RedisMultiCache::getInstance($redisCfgName);
        } else {
            $redis = \Redis\RedisMultiCache::getInstance($redisCfgName)->partitionByUID($partition);
        }
        if ($redis->set($redisKey, $expireAt, ['NX', 'EX' => $realExpireAfterSeconds])) {
            // $redis->expireAt($redisKey, $expireAt);
            $result = true;
        } elseif ($timestamp > $redis->get($redisKey) && $timestamp > $redis->getSet($redisKey, $expireAt)) {
            $redis->expire($redisKey, $realExpireAfterSeconds);
            $result = true;
        }
        return $result;
    }

    /**
     * 设置锁使用的分区.
     *
     * @param integer $partition 分区标识.
     *
     * @return \Utils\Locker\Redis
     */
    public function partition($partition)
    {
        $this->partition = $partition;
        return $this;
    }

    /**
     * 获取当前设置的分区, 获取之后会清空属性上的分区设置.
     *
     * @return integer
     */
    public function getPartition()
    {
        $partition = $this->partition;
        $this->partition = null;
        return $partition;
    }

    /**
     * 生成redis的key.
     *
     * @return string
     */
    private function genKey()
    {
        return \Config\Locker::$redis['prefix'] . '_' . md5(implode('_', func_get_args()));
    }

    /**
     * 解锁.
     *
     * @param string $prefix 锁前缀.
     * @param string $key    锁的key.
     *
     * @return integer 解锁成功返回1, 没有需要解锁的返回0.
     */
    public function unlock($prefix, $key)
    {
        $partition = $this->getPartition();// 保险起见在第一行就重置$this->partition为null.
        $redisCfgName = \Config\Locker::$redis['name'];
        if (is_null($partition)) {
            $redis = \Redis\RedisMultiCache::getInstance($redisCfgName);
        } else {
            $redis = \Redis\RedisMultiCache::getInstance($redisCfgName)->partitionByUID($partition);
        }
        return $redis->del($this->genKey($prefix, $key));
    }

    /**
     * 自动执行加锁和解锁过程.
     *
     * @param string   $prefix       锁前缀.
     * @param string   $key          锁的key.
     * @param integer  $partitionUid 分区UID.
     * @param integer  $expireAfrer  多久过期,默认为0,即300秒(5分钟)后过期.
     * @param string   $message      锁定后的异常信息.
     * @param callable $func         需要锁定的执行的匿名函数.
     * @param integer  $loglevel     日志记录方式, 1=记录正常返回日志, 2=记录异常日志, 3=全部记录.
     *
     * @return mixed
     * @throws \Exception 异常.
     */
    public function auto($prefix, $key, $partitionUid, $expireAfrer, $message, $func, $loglevel = 3)
    {
        if (!$message) {
            $message = 'system is processing, please wait a moment';
        }
        try {
            if (!$this->partition($partitionUid)->lock($prefix, $key, $expireAfrer)) {
                throw new \RpcBusinessException($message, 99999);
            }
            $result = $func();
            $this->partition($partitionUid)->unlock($prefix, $key);
            if (($loglevel & 1) == 1) {
                \Utils\Log\Logger::instance()->log($result, 'jsonfile', 'Y-m-d', 3);
            }
            return $result;
        } catch (\Exception $e) {
            if ($e->getCode() != 99999) {
                // 不是由于加锁失败的异常code都需要解锁, 因为流程正常处理结束了.
                $this->partition($partitionUid)->unlock($prefix, $key);
            }
            if (($loglevel & 2) == 2) {
                \Utils\Log\Logger::instance()->log($e, 'jsonfile', 'Y-m-d', 3);
            }
            throw $e;
        }
    }

}
