<?php
/**
 * 各种公用方法.
 *
 * @author: dengjing<jingd3@jumei.com>.
 */
namespace Utils;

/**
 * 各种公用方法.
 */
class Util extends Singleton {

    /**
     * Get instance of the derived class.
     *
     * @return $this
     */
    public static function instance()
    {
        return parent::instance();
    }

    /**
     * 通过域名判断应用的名称.
     *
     * @param string $host 请求的域名,如m.shuabuapp.com, m.rd.shuabuapp.com.
     *
     * @return string 应用名称,匹配不到返回空字符.
     */
    public function getAppNameByCfg($host)
    {
        $result = '';
        preg_match('@\w+\.(com\.cn|net\.cn|com|net|cn|org|gov|edu|info|vip|club|mobi|me|xyz)$@', $host, $matches);
        $domain = $matches[0] ?? '';
        if (!empty($host)) {
            $cfg = \Config\AppHost::$mapping ?? [];
            foreach ($cfg as $appName => $domainList) {
                if (in_array($domain, $domainList)) {
                    $result = $appName;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * 设置PHPClient和PHPServer之间的全局上下文内容.
     *
     * @param string $key   键名.
     * @param mixed  $value 设置的值.
     *
     * @return $this
     */
    public function setContext($key, $value)
    {
        global $context;
        $context[$key] = $value;
        return $this;
    }

    /**
     * 通过key获取PHPClient和PHPServer之间的全局上下文内容.
     *
     * @param string $key     键名.
     * @param string $default 没有设置时的默认值.
     *
     * @return mixed
     */
    public function getContext($key, $default = '')
    {
        global $context;
        return $context[$key] ?? $default;
    }

    /**
     * 通过utm_source和设备id或uid来做ab分组.
     *
     * @param array   $device 设备信息数组,需要包含platform,client_v,device_id,utm_source,uidlast5.
     * @param integer $uid    用户uid.
     * @param array   $cfg    AB分组配置.
     * <pre>
     *   [
     *       'android' => [
     *           'oppo' => [
     *              'a' => [
     *                  'whitelist' => 'bfefde49532ed0860b700feb1acfaea0,432003',
     *                  'device_id' => '1*|2*',
     *                  'uid' => '*3|*4',
     *                  'client_v' => '1.500~1.505|1.600~|~1.400|1.450'
     *              ]
     *          ]
     *      ]
     *  ];
     * </pre>
     *
     * @return string|boolean 命中的分组名字, 返回空字符表示渠道有分组配置但是未命中分组, 返回false表示渠道没有配置分组.
     */
    public function abTestByUtmSource($device, $uid, $cfg = [])
    {
        $platform = strtolower($device['platform'] ?? '');
        $deviceId = $device['device_id'] ?? '';
        $utmSource = $device['utm_source'] ?? '';
        $config = [];
        $matchUtm = '';
        if (isset($cfg[$platform]['*'])) {
            // 优先检查渠道用*表示所有渠道都设置了分组.
            $config = $cfg[$platform]['*'];
            $matchUtm = '*';
        } elseif (isset($cfg[$platform][$utmSource])) {
            // 其次检查某个指定的渠道有设置分组.
            $config = $cfg[$platform][$utmSource];
            $matchUtm = $utmSource;
        } elseif (!empty($cfg[$platform])) {
            // 最后循环检查是否有通配符能匹配的渠道, 比如chuanshanjia*能匹配chuanshanji_1,chuanshanjia_2,chuanshanjia_3.
            foreach ($cfg[$platform] as $utm => $eachGrp) {
                if (strpos($utm, '*') !== false) {
                    $utmExp = str_replace('*', '\w*', $utm);
                    if (preg_match('@' . $utmExp . '@', $utmSource)) {
                        $config = $eachGrp;
                        $matchUtm = $utm;
                        break;
                    }
                }
            }
        }
        $clientV = $device['client_v'] ?? '';
        $result = false;
        if ($config) {
            $result = '';
            foreach ($config as $grp => $setting) {
                if (($whitelist = !empty($setting['whitelist']) ? explode(',', $setting['whitelist']) : []) && (($deviceId && in_array($deviceId, $whitelist)) || ($uid && in_array($uid, $whitelist)))) {
                    // 设备id或uid在白名单中, 不管其他条件优先命中.
                    $result = $grp;
                    break;
                }
            }
            if (!$result) {
                foreach ($config as $grp => $setting) {
                    if (($blacklist = !empty($setting['blacklist']) ? explode(',', $setting['blacklist']) : []) && (($deviceId && in_array($deviceId, $blacklist)) || ($uid && in_array($uid, $blacklist)))) {
                        // 设备id或uid在黑名单中, 不管其他条件跳过.
                        continue;
                    }
                    if (($weight = !empty($setting['weight']) ? $setting['weight'] : '') && $weight != '*' && !in_array($device['weight'], explode('|', $weight))) {
                        // 限制是否迷你包或者全量包, weight不设置或设置成*就是所有包都命中, 也可以设置成S|L.
                        continue;
                    }
                    if (($clientVRange = !empty($setting['client_v']) ? explode('|', $setting['client_v']) : [])) {
                        // 分组中设置了版本限制.
                        if (!$clientV) {
                            // 没有传版本号无法命中该分组.
                            continue;
                        }
                        $clientVNew = $clientV * 100000;
                        $hitVersion = false;
                        foreach ($clientVRange as $each) {
                            list($min, $max) = strpos($each, '~') !== false ? explode('~', $each) : [$each, $each];
                            $min = empty($min) ? $clientVNew - 1 : $min * 100000;
                            $max = empty($max) ? $clientVNew + 1 : $max * 100000;
                            if ($min <= $clientVNew && $clientVNew <= $max) {
                                $hitVersion = true;
                                break;
                            }
                        }
                        if (!$hitVersion) {
                            // 参数的版本没有命中分组的版本范围.
                            continue;
                        }
                    }
                    if ($deviceId && ($deviceIdExp = !empty($setting['device_id']) ? str_replace(['*', ','], ['\d', '|'], $setting['device_id']) : '')) {
                        $last2 = substr(sprintf('%u', crc32($deviceId)), -2);
                        if (preg_match('@' . $deviceIdExp . '@', $last2)) {
                            $result = $grp;
                            break;
                        }
                    }
                    if ($uid && ($uidExp = !empty($setting['uid']) ? str_replace(['*', ','], ['\d', '|'], $setting['uid']) : '')) {
                        if (!empty($device['uidlast5'])) {
                            $last2 = substr($uid, -5, 1) . substr($uid, -2, 1);
                        } else {
                            $last2 = substr($uid, -2);
                        }
                        if (preg_match('@' . $uidExp . '@', $last2)) {
                            $result = $grp;
                            break;
                        }
                    }
                }
            }
        }
        if (isset($device['return_arr']) && $device['return_arr']) {
            $result = [
                'result'        => $result,
                'utm_source'    => $matchUtm,
                'group'         => $result ? $config[$result] : [],
            ];
        }
        return $result;
    }

    /**
     * 通过AB规则来替换默认的配置属性(会unset掉AB规则的内容).
     *
     * @param integer $uid     用户uid.
     * @param array   $device  设备信息数组.
     * @param array   $cfg     配置内容.
     * @param string  $ruleKey AB规则对应的数组key名字.
     *
     * @return array
     */
    public function replaceCfgByRule($uid, $device, $cfg, $ruleKey = 'rule')
    {
        if (!empty($cfg[$ruleKey])) {
            $device['return_arr'] = 1;
            $fields = array_diff(array_keys($cfg), [$ruleKey]); // 排除rule字段以外的所有字段.
            foreach ($cfg[$ruleKey] as $rule) {
                if (($res = \Utils\Util::instance()->abTestByUtmSource($device, $uid, $rule)) && $res['result']) {
                    // 能命中其规则就替换默认值.
                    foreach ($fields as $key) {
                        if (isset($res['group'][$key])) {
                            $cfg[$key] = $res['group'][$key];
                        } elseif (isset($rule[$key])) {
                            $cfg[$key] = $rule[$key];
                        }
                    }
                    break;
                }
            }
            unset($cfg[$ruleKey]);
        }
        return $cfg;
    }

}
