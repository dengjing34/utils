<?php
/**
 * 人脸核身示例文件.
 *
 * @author jianyoun <jianyoun@jumei.com>
 */

date_default_timezone_set('Asia/Shanghai');
require_once __DIR__ . '/../../Vendor/Bootstrap/Autoloader.php';
\Bootstrap\Autoloader::instance()->addRoot(__DIR__  . '/../../../../')->init();
require_once __DIR__ . '/../Base.php';
require_once __DIR__ . '/../H5.php';
require_once __DIR__ . '/../Config/TencentCloud.php';
require_once __DIR__ . '/../Config/Redis.php';
require_once __DIR__ . '/../Config/Log.php';

$params = [
    'orderNo' => 1002,
    'name' => '聂建友',
    'idNo' => 511024198503031713,
    'userId' => 20001,
    'sourcePhotoStr' => '',
//    'sourcePhotoType' => '2',
];
$res = \Utils\TencentCloud\H5::instance('shuabao')->getH5FaceId($params);
var_dump($res);

// 启动人脸核身.
$params = [
    'orderNo' => 1002,
    'h5faceId' => $res['h5faceId'],
    'url' => 'http://h5.rd.shuabaola.cn/tencent_auth',
    'resultType' => 1,
    'userId' => 20001,
    'from' => 'browser',
    'redirectType' => 1,
];
$res = \Utils\TencentCloud\H5::instance('shuabao')->getH5FaceUrl($params);
var_dump($res);

// 验证结果.
$params = [
    'order_no' => 1001,
    'get_file' => 0,
];
$res = \Utils\TencentCloud\H5::instance('shuabao')->checkH5FaceAuth($params);
var_dump($res);