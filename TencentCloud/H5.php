<?php
/**
 * @author: jianyoun<jianyoun@jumei.com>.
 *
 */
namespace Utils\TencentCloud;

class H5 extends Base
{
    const JSAPI_TICKET_PREFIX = "jsapi_ticket_";
    const LOCK_REFRESH_JS_TICKET_PREFIX = "refresh_jsticket_lock_";

    /**
     * Get instance of the derived class.
     *
     * @param string $endpoint 配置的endpoint.
     *
     * @return \Utils\TencentCloud\H5
     */
    public static function instance($endpoint)
    {
        return parent::instance($endpoint);
    }

    /**
     * 上送身份信息.
     *
     * @param array $params 参数.
     *
     * @link https://cloud.tencent.com/document/product/1007/35883
     *
     * @return array
     */
    public function getH5FaceId($params)
    {
        $url = "https://idasc.webank.com/api/server/h5/geth5faceid";
        $params['webankAppId'] = $this->config['app_id'];
        $params['version'] = $this->config['version'];
        $ticket = $this->getTicket();
        if (empty($ticket)) {
            return [];
        }
        $params['ticket'] = $ticket;
        $params['sign'] = $this->getSign([$params['webankAppId'], $params['orderNo'], $params['name'], $params['idNo'], $params['userId'], $params['version'], $params['ticket']]);
        $result = $this->postjson($url, json_encode($params), ['Content-Type: application/json']);
        return !empty($result['result']) ? $result['result'] : [];
    }

    /**
     * 启动 H5 人脸核身.
     *
     * @param array $params 参数.
     *
     * @link https://cloud.tencent.com/document/product/1007/35884
     *
     * @return string
     */
    public function getH5FaceUrl($params)
    {
        $url = "https://ida.webank.com/api/web/login";
        $params['webankAppId'] = $this->config['app_id'];
        $params['version'] = $this->config['version'];
        $params['nonce'] = $this->createNonceStr(32);
        $ticket = $this->getNonceTicket($params['userId']);
        if (empty($ticket)) {
            return '';
        }
        $params['sign'] = $this->getSign([$params['webankAppId'], $params['orderNo'], $params['userId'], $params['nonce'], $params['h5faceId'], $params['version'], $ticket]);
        $getString = http_build_query($params);
        $url .= '?' . $getString;
        return $url;
    }

    /**
     * 检查是否认证成功.
     *
     * @param array $params 参数.
     *
     * @return array
     */
    public function checkH5FaceAuth($params)
    {
        $url = "https://idasc.webank.com/api/server/sync";
        $params['app_id'] = $this->config['app_id'];
        $params['version'] = $this->config['version'];
        $params['nonce'] = $this->createNonceStr(32);
        $ticket = $this->getTicket();
        if (empty($ticket)) {
            return [];
        }
        $params['sign'] = $this->getSign([$params['app_id'], $params['order_no'], $params['nonce'], $params['version'], $ticket]);
        return $this->getjson($url, $params);
    }

}
