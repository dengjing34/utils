<?php
/**
 * @author: jianyoun<jianyoun@jumei.com>.
 *
 */
namespace Utils\TencentCloud;

abstract class Base {

    const ACCESS_TOKEN_PREFIX = 'tencent_cloud_access_token_cache_';

    const TICKET_PREFIX = 'tencent_cloud_ticket_cache_';
    /**
     * Singleton Instances.
     *
     * @var array
     */
    public static $instances = [];
    /**
     * 相关的配置信息
     *
     * @var array
     */
    protected $config = [];
    /**
     * 应用的名字.
     *
     * @var string
     */
    protected $endpoint;
    /**
     * 配置的key名称,如h5,applet.
     *
     * @var string
     */
    protected $platform;
    /**
     * redis的配置名.
     *
     * @var string
     */
    protected static $redisEndPoint;

    /**
     * not support new class.
     *
     * @param string $endpoint
     *
     * @throws \Exception
     */
    private function __construct($endpoint) {
        $cfg = (array) new \Config\TencentCloud();
        $namespace = __NAMESPACE__ ;
        $this->platform = strtolower(str_replace($namespace . '\\', '', get_called_class()));
        $this->endpoint = $endpoint;
        if (empty($cfg[$this->platform][$this->endpoint])) {
            throw new \Exception($this->platform . ' config not set');
        }
        if (empty($cfg['redis'])) {
            throw new \Exception('redis config not set');
        }
        static::$redisEndPoint = $cfg['redis'];
        $this->config = $cfg[$this->platform][$this->endpoint];
        if (!isset($this->config['app_id'])) {
            $this->config['app_id'] = $this->config['appid'];
        }
        if (!isset($this->config['app_secret'])) {
            $this->config['app_secret'] = $this->config['appsecrect'];
        }
    }


    /**
     * Get instance of the derived class.
     *
     * @param string $endpoint 配置的endpoint.
     *
     * @return \Utils\TencentCloud\Base
     */
    public static function instance($endpoint)
    {
        $className = get_called_class();
        if (!isset(self::$instances[$className][$endpoint])) {
            static::$instances[$className][$endpoint] = new $className($endpoint);
        }
        return static::$instances[$className][$endpoint];
    }

    /**
     * 获取配置.
     *
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * http get请求.
     *
     * @param string $url     请求的url.
     * @param array  $get     get参数.
     * @param array  $headers header参数.
     *
     * @return string
     */
    protected function get($url, $get = [], $headers = [])
    {
        return $this->request($url, $get, $headers);
    }

    /**
     * http post 请求.
     *
     * @param string $url     请求的url.
     * @param string $post    post参数.
     * @param array  $headers header参数.
     *
     * @return string
     */
    protected function post($url, $post, $headers = [])
    {
        return $this->request($url, [], $post, $headers);
    }

    /**
     * 执行http/https请求.
     *
     * @param string $url  请求的url.
     * @param array  $get  get参数.
     * @param array  $post post参数.
     *
     * @return string
     */
    protected function request($url, $get = [], $post = [], $headers = [])
    {
        if (!empty($get)) {
            $getString = http_build_query($get);
            $url .= '?' . $getString;
        }

        $ch = curl_init($url);
        if (!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        if (!empty($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        if ($info['http_code'] != 200 || curl_error($ch) != '') {
            $result = false;
        } else {
            $result = $response;
        }
        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 4);
        $functions = ['get', 'getjson', 'post', 'postjson'];
        $action = '';
        for ($i = 1; $i <= 3; $i++) {
            if (!in_array($backtrace[$i]['function'], $functions)) {
                $action = $backtrace[$i]['function'];
                break;
            }
        }
        $logName = 'tencent_cloud_req_' . $action;
        $logCfg = \Log\Handler::config();
        if (empty($logCfg)) {
            $logCfg = (array) new \Config\Log;
            \Log\Handler::config($logCfg);
        }
        if (!isset($logCfg[$logName])) {
            $logCfg[$logName] = ['logger' => 'jsonfile', 'rotateFormat' => 'Y-m-d'];
            \Log\Handler::config($logCfg);
        }
        \Log\Handler::instance($logName)->log(['date' => date('Y-m-d H:i:s'), 'platform' => $this->platform, 'app' => $this->endpoint, 'action' => $action, 'url' => $url, 'response' => $result, 'get' => $get, 'post' => $post]);
        curl_close($ch);
        if ($result === false) {
            throw new \Exception(var_export($info, true));
        }
        return $result;
    }

    /**
     * get请求获取json数据.
     *
     * @param string $url     请求地址.
     * @param array  $get     参数.
     * @param array  $headers header参数.
     *
     * @return array
     */
    protected function getjson($url, $get = [], $headers = [])
    {
        return json_decode($this->get($url, $get, $headers), true);
    }

    /**
     * post请求获取json数据.
     *
     * @param string $url     请求地址.
     * @param array  $post    参数.
     * @param array  $headers header参数.
     *
     * @return array
     */
    protected function postjson($url, $post = [], $headers = [])
    {
        return json_decode($this->post($url, $post, $headers), true);
    }

    /**
     * 获取redis实例.
     *
     * @return \Redis\RedisCache
     */
    protected function redis()
    {
        return \Redis\RedisMultiCache::getInstance(static::$redisEndPoint);
    }

    /**
     * 优先从缓存中获取公用token, 缓存中没有会去微信重新获取.
     *
     * @return string 成功返回access_token,失败返回空.
     */
    public function getAccesstoken()
    {
        $redisKey = self::ACCESS_TOKEN_PREFIX . md5($this->config['app_id'] . $this->config['app_secret'] . $this->config['version']);
        $token = $this->redis()->get($redisKey);
        if (!$token) {
            $token = $this->refreshAccessToken();
        }
        return $token;
    }

    /**
     * 调用腾讯接口重新获取公用token.
     *
     * @return string 成功返回access_token,失败返回空.
     * @link https://cloud.tencent.com/document/product/1007/37304
     */
    public function refreshAccessToken()
    {
        $token = '';
        $appId = $this->config['app_id'];
        $appSecret = $this->config['app_secret'];
        $redisKey = self::ACCESS_TOKEN_PREFIX . md5($appId . $appSecret . $this->config['version']);
        $url = "https://idasc.webank.com/api/oauth2/access_token";
        $retry = 3;
        while ($retry--) {
            $result = $this->getjson($url, ['grant_type' => 'client_credential', 'app_id' => $appId, 'secret' => $appSecret, 'version' => $this->config['version']]);
            if (!empty($result['access_token'])) {
                $token = $result['access_token'];
                $expire = intval($result['expire_in']) - 600;
                $this->redis()->setex($redisKey, $expire, $token);
                break;
            }
        }
        return $token;
    }

    /**
     * 验证消息的确来自微信服务器.
     *
     * @param string $signature 签名.
     * @param string $timestamp 请求时间戳.
     * @param string $nonce     随机数.
     *
     * @return boolean 返回true表示校验通过,false表示不通过.
     */
    public function checkSignature($signature, $timestamp, $nonce)
    {
        $token = $this->config['token'];
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );
        if ($tmpStr == $signature ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 生成随机字符串.
     *
     * @param integer $length 长度.
     *
     * @return string
     */
    public function createNonceStr($length = 16)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    /**
     * 获取腾讯ticket.
     *
     * @return string 成功返回ticket,失败返回空.
     * @link https://cloud.tencent.com/document/product/1007/37305
     */
    public function getTicket()
    {
        $ticket = '';
        $token = $this->getAccesstoken();
        if (!empty($token)) {
            $redisKey = self::TICKET_PREFIX . $token;
            $ticket = $this->redis()->get($redisKey);
            if (!$ticket) {
                $ticket = $this->refreshTicket($token);
            }
        }
        return $ticket;
    }

    /**
     * 刷新ticket.
     *
     * @return string
     */
    public function refreshTicket($token)
    {
        $ticket = '';
        $redisKey = self::TICKET_PREFIX . $token;
        $url = "https://idasc.webank.com/api/oauth2/api_ticket";
        $retry = 3;
        while ($retry--) {
            $result = $this->getjson($url, ['app_id' => $this->config['app_id'], 'access_token' => $token, 'type' => 'SIGN', 'version' => $this->config['version']]);
            if (!empty($result['tickets'][0]['value'])) {
                $ticket = $result['tickets'][0]['value'];
                $expire = intval($result['tickets'][0]['expire_in']) - 600;
                $this->redis()->setex($redisKey, $expire, $ticket);
                break;
            }
        }
        return $ticket;
    }

    /**
     * 生成签名.
     *
     * @param array $params 签名数组.
     *
     * @return string
     */
    public function getSign($params)
    {
        sort($params, SORT_STRING);
        $tmpStr = implode( $params );
        return sha1( $tmpStr );
    }

    /**
     * 获取nonce ticket.
     *
     * @param integer $userId 用户ID.
     *
     * @return string
     */
    public function getNonceTicket($userId)
    {
        $ticket = '';
        $token = $this->getAccesstoken();
        if (!empty($token)) {
            $url = "https://idasc.webank.com/api/oauth2/api_ticket";
            $retry = 3;
            while ($retry--) {
                $result = $this->getjson($url, ['app_id' => $this->config['app_id'], 'access_token' => $token, 'type' => 'NONCE', 'version' => $this->config['version'], 'user_id' => $userId]);
                if (!empty($result['tickets'][0]['value'])) {
                    $ticket = $result['tickets'][0]['value'];
                    break;
                }
            }
        }
        return $ticket;
    }

}
