<?php
/**
 * 版本控制检查.
 *
 * @author shangyuh<shangyuh@jumei.com>
 */
namespace Utils\VersionCheck;

/**
 * Class VersionCheck.
 */
class VersionCheck extends \Utils\Singleton {

    /**
     * Get instance of the derived class.
     *
     * @return \Utils\VersionCheck\VersionCheck
     */
    public static function instance()
    {
        return parent::instance();
    }

    /**
     * 将clientV转化成整数.
     * 使用方式\Utils\VersionCheck\VersionCheck::clientVInt($clientV, $clientVCompareCardinalNumber).
     *
     * @param string  $clientV                      版本号.
     * @param integer $clientVCompareCardinalNumber 版本比较放大系数.
     *
     * @return integer
     */
    public static function clientVInt($clientV = '', $clientVCompareCardinalNumber = 1000)
    {
        $clientVInt = 0;
        $clientVCompareCardinalNumber = is_numeric($clientVCompareCardinalNumber) ? $clientVCompareCardinalNumber : 1000;
        if (!empty($clientV)) {
            $clientVInt = $clientV * $clientVCompareCardinalNumber;
        }
        return $clientVInt;
    }

    /**
     * 通过应用版本号或设备控制检查是否启用某功能(默认未启用).
     * 使用方式\Utils\VersionCheck\VersionCheck::verify($platform, $clientV, $cfgInfo, $clientVCompareCardinalNumber, $extInfo, $defaultValue).
     *
     * @param string $platform                      平台.
     * @param string $clientV                       应用版本号.
     * @param array  $cfgInfo                       某信息配置信息.
     * <pre>
     * $cfgInfo = array(
     *   // 版本控制 全版本"~";指定版本"1.500";版本区间"1.500~1.600";小于等于某版本"~1.500";大于等于某版本"1.500~"(满足其中一个配置即可).
     *   // iOS控制信息.
     *   'ios'     => array(
     *       // 第一个判断条件.
     *       0 => '~1.700',
     *       // 第二个判断条件.(相互独立，不与第一个判断条件冲突).
     *       1 => '1.500~1.501',
     *       // 第三个判断条件.(相互独立，不与第一个、第二个判断条件冲突).
     *       2 => '1.800',
     *   ),
     *   // Android控制信息.
     *   'android' => array(
     *       // 第一个判断条件.
     *       0 => '2.210',
     *       // 第二个判断条件.(相互独立，不与第一个判断条件冲突).
     *       1 => '~2.100',
     *   ),
     *   // mini包iOS控制信息.
     *   'mini_ios'     => array(
     *       // 第一个判断条件.
     *       0 => '~1.700',
     *       // 第二个判断条件.(相互独立，不与第一个判断条件冲突).
     *       1 => '1.500~1.501',
     *       // 第三个判断条件.(相互独立，不与第一个、第二个判断条件冲突).
     *       2 => '1.800',
     *   ),
     *   // mini包Android控制信息.
     *   'mini_android' => array(
     *       // 第一个判断条件.
     *       0 => '2.210',
     *       // 第二个判断条件.(相互独立，不与第一个判断条件冲突).
     *       1 => '~2.100',
     *   ),
     *   // uid白名单(白名单优先级高于黑名单;uid优先级高于device_id).
     *   'uid_whitelist' => array(
     *       0 => 1234567,
     *       1 => 12345678,
     *   ),
     *   // uid黑名单(白名单优先级高于黑名单;uid优先级高于device_id).
     *   'uid_blacklist' => array(
     *       0 => 2345678,
     *       1 => 23456789,
     *   ),
     *   // device_id白名单(白名单优先级高于黑名单;uid优先级高于device_id).
     *   'device_id_whitelist' => array(
     *       0 => 'abcdefg',
     *       1 => 'abcdefgh',
     *   ),
     *   // device_id黑名单(白名单优先级高于黑名单;uid优先级高于device_id).
     *   'device_id_blacklist' => array(
     *       0 => 'bcdefgh',
     *       1 => 'bcdefghi',
     *   ),
     * )
     * </pre>
     * @param integer $clientVCompareCardinalNumber 版本比较放大系数.
     * @param array   $extInfo                      扩展信息,用于额外条件判断(1.uid/device_id白名单;2.uid/device_id黑名单;3.is_mini是否是mini包).
     * <pre>
     * $extInfo = array(
     *   // 用户ID.
     *   'uid'       => 1234567,
     *   // 设备ID.
     *   'device_id' => 'abcdefg',
     *   // 是否是mini包.
     *   'is_mini' => 1,
     * )
     * </pre>
     * @param boolean $defaultValue                 默认返回的是否启用的状态.
     *
     * @return array
     * <pre>
     * array(
     *   // 返回的是否启用状态(直接用此值判断即可).
     *   // 启用:true.
     *   // 未启用:false.
     *   'status' => true,
     *   // 判断的结果依据值(用于方便获悉判断的依据).
     *   // -3:平台不在允许列表中.
     *   // -2:配置信息为空.
     *   // -1:平台或应用版本号为空.
     *   // 0:配置信息中没有该平台控制.
     *   // 1:uid白名单.
     *   // 2:uid黑名单.
     *   // 3:device_id白名单.
     *   // 4:device_id黑名单.
     *   // 5:配置信息中有该平台控制但没有命中版本区间规则.
     *   // 6:命中配置信息中某个版本区间.
     *   // 7:命中配置信息中某个特定版本.
     *   'code'   => 0,
     * )
     * </pre>
     */
    public static function verify($platform, $clientV, array $cfgInfo = array(), $clientVCompareCardinalNumber = 1000, array $extInfo = array(), $defaultValue = false)
    {
        // 转化默认返回的是否启用的状态.
        $defaultValue = empty($defaultValue) ? false : true;
        $res = array(
            'status' => $defaultValue,
            'code'   => 0,
        );
        // 平台或应用版本号为空.
        if (empty($platform) || empty($clientV)) {
            $res['code'] = -1;
            return $res;
        }
        // 某信息配置信息为空.
        if (empty($cfgInfo)) {
            $res['code'] = -2;
            return $res;
        }
        // 转化版本比较放大系数.
        $clientVCompareCardinalNumber = is_numeric($clientVCompareCardinalNumber) ? $clientVCompareCardinalNumber : 1000;
        // 判断黑白名单.
        if (!empty($extInfo)) {
            // 判断uid黑白名单.
            if (!empty($extInfo['uid'])) {
                // 验证白名单.
                if (!empty($cfgInfo['uid_whitelist'])
                    && is_array($cfgInfo['uid_whitelist'])
                    && in_array($extInfo['uid'], $cfgInfo['uid_whitelist'])) {
                    $res['status'] = true;
                    $res['code'] = 1;
                    return $res;
                }
                // 验证黑名单.
                if (!empty($cfgInfo['uid_blacklist'])
                    && is_array($cfgInfo['uid_blacklist'])
                    && in_array($extInfo['uid'], $cfgInfo['uid_blacklist'])) {
                    $res['status'] = false;
                    $res['code'] = 2;
                    return $res;
                }
            }
            // 判断device_id黑白名单.
            if (!empty($extInfo['device_id'])) {
                // 验证白名单.
                if (!empty($cfgInfo['device_id_whitelist'])
                    && is_array($cfgInfo['device_id_whitelist'])
                    && in_array($extInfo['device_id'], $cfgInfo['device_id_whitelist'])) {
                    $res['status'] = true;
                    $res['code'] = 3;
                    return $res;
                }
                // 验证黑名单.
                if (!empty($cfgInfo['device_id_blacklist'])
                    && is_array($cfgInfo['device_id_blacklist'])
                    && in_array($extInfo['device_id'], $cfgInfo['device_id_blacklist'])) {
                    $res['status'] = false;
                    $res['code'] = 4;
                    return $res;
                }
            }
        }
        // 转化客户端系统和版本号.
        $platform = strtolower($platform);
        $clientVersion = self::clientVInt($clientV, $clientVCompareCardinalNumber);
        if (empty($platform)
            || !in_array($platform, array('ios', 'android'))
            || empty($clientVersion)) {
            $res['code'] = -3;
            return $res;
        }
        // 转换客户端平台是mini包还是普通包.
        if (!empty($extInfo['is_mini'])) {
            $platform = 'mini_' . $platform;
        }
        // 判断版本.
        if (!empty($cfgInfo[$platform]) && is_array($cfgInfo[$platform])) {
            $res['code'] = 5;
            $cfg = $cfgInfo[$platform];
            foreach ($cfg as $versPairs) {
                if (strpos($versPairs, '~') !== false) {
                    list($min, $max) = explode('~', $versPairs);
                    $minC = empty($min) ? $clientVersion - 1 : self::clientVInt($min, $clientVCompareCardinalNumber);
                    $maxC = empty($max) ? $clientVersion + 1 : self::clientVInt($max, $clientVCompareCardinalNumber);
                    if ($minC <= $clientVersion && $clientVersion <= $maxC) {
                        $res['status'] = true;
                        $res['code'] = 6;
                        break;
                    }
                } elseif (is_numeric($versPairs)) {
                    $ver = self::clientVInt($versPairs, $clientVCompareCardinalNumber);
                    if ($ver == $clientVersion) {
                        $res['status'] = true;
                        $res['code'] = 7;
                        break;
                    }
                }

            }
            return $res;
        } else {
            return $res;
        }
    }

}
