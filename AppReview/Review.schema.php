<?php
namespace Config;

class Review
{

    /**
     * 审核中的APP版本平台渠道配置.
     *
     * @var array
     */
    public static $cfg = array(
        'ios' => array(
            'appstore' => array('0.001'),
        ),
        'android' => array(
            'oppo' => array('1.201'),
            'huawei' => array('1.201'),
            '*' => array('1.201'),
        )
    );

}