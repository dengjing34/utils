<?php
/**
 * APP审核版本验证器
 *
 * @author: dengjing<jingd3@jumei.com>.
 */
namespace Utils\AppReview;

/**
 * APP审核版本验证器.
 */
class App extends \Utils\Singleton {

    /**
     * Get instance of the derived class.
     *
     * @return $this
     */
    public static function instance()
    {
        return parent::instance();
    }

    /**
     * 检查某个渠道下的版本是否处于应用市场审核中.
     *
     * @param string $platform  平台,iOS或Android.
     * @param string $version   客户端版本号.
     * @param string $utmsource 客户端的来源渠道.
     *
     * @return boolean 返回true=审核中, false=不在审核中.
     */
    public function isReviewing($platform, $version, $utmsource)
    {
        $result = false;
        $platform = strtolower($platform);
        $version = (string)$version;
        if ($cfg = \Config\Review::$cfg[$platform] ?? []) {
            foreach ($cfg as $source => $reviewVersions) {
                if (($source == $utmsource || $source == '*') && in_array($version, $reviewVersions)) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * 检查某个渠道下的版本是否处于应用市场审核中(用于广告返回数据判断).
     *
     * @param string $platform  平台,iOS或Android.
     * @param string $version   客户端版本号.
     * @param string $utmsource 客户端的来源渠道.
     *
     * @return boolean 返回true=审核中, false=不在审核中.
     */
    public function isReviewingToAd($platform, $version, $utmsource)
    {
        $result = false;
        $platform = strtolower($platform);
        $platform = 'ad_' . $platform;
        $version = (string)$version;
        if ($cfg = \Config\Review::$cfg[$platform] ?? []) {
            foreach ($cfg as $source => $reviewVersions) {
                if (($source == $utmsource || $source == '*') && in_array($version, $reviewVersions)) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

}
